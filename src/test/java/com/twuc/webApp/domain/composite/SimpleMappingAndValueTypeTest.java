package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMappingAndValueTypeTest extends JpaTestBase{

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

    private JpaTestBase jpaTestBase;

    @Test
    void should_save_and_read_company_profile() {
        ClosureValue<Long> expectId = new ClosureValue<>();
        flushAndClear(em->{
            CompanyProfile companyProfile = companyProfileRepository.save(new CompanyProfile("Xi'an", "Zhangba"));
            expectId.setValue(companyProfile.getId());
        });

        run(em ->{
            CompanyProfile companyProfile = companyProfileRepository.findById(expectId.getValue()).orElseThrow(RuntimeException::new);
            assertEquals("Xi'an", companyProfile.getCity());
            assertEquals("Zhangba", companyProfile.getStreet());
        });
    }

    @Test
    void should_save_and_read_user_profile() {
        ClosureValue<Long> expectId = new ClosureValue<>();
        flushAndClear(em->{
            UserProfile companyProfile = userProfileRepository.save(new UserProfile("Xi'an", "Zhangba"));
            expectId.setValue(companyProfile.getId());
        });

        run(em ->{
            UserProfile userProfile = userProfileRepository.findById(expectId.getValue()).orElseThrow(RuntimeException::new);
            assertEquals("Xi'an", userProfile.getAddressCity());
            assertEquals("Zhangba", userProfile.getAddressStreet());
        });
    }
}
